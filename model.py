import datetime
from pathlib import Path

import joblib
import pandas as pd
import yfinance as yf
from fbprophet import Prophet

BASE_DIR = Path(__file__).resolve(strict=True).parent
TODAY = datetime.date.today()


def train(ticker="MSFT"):
    # data = yf.download("^GSPC", "2008-01-01", TODAY.strftime("%Y-%m-%d"))
    data = yf.download(ticker, "2020-01-01", TODAY.strftime("%Y-%m-%d"))
    data.head()
    data["Adj Close"].plot(title=f"{ticker} Stock Adjusted Closing Price")

    df_forecast = data.copy()
    # don't work with date index but with an ordered index 
    df_forecast.reset_index(inplace=True)
    # crerate new columns (ds and y) in df_forecast
    df_forecast["ds"] = df_forecast["Date"]
    df_forecast["y"] = df_forecast["Adj Close"]
    # create a new df with only ds and y columns
    df_forecast = df_forecast[["ds", "y"]]
    df_forecast

    model = Prophet()
    model.fit(df_forecast)

    joblib.dump(model, Path(BASE_DIR).joinpath(f"{ticker}.joblib"))


def predict(ticker="MSFT", days=7):
    model_file = Path(BASE_DIR).joinpath(f"{ticker}.joblib")
    if not model_file.exists():
        return False

    model = joblib.load(model_file)
    
    future = TODAY + datetime.timedelta(days=days)
    
    # df with datetimeIndex from 2020-01-01 to today+7 days
    dates = pd.date_range(start="2020-01-01", end=future.strftime("%m/%d/%Y"),)
    #create df with only dates
    df = pd.DataFrame({"ds": dates})

    # forecast will have a lot of columns like ds
    # but also trends, yhat_lower, weekly and so on
    forecast = model.predict(df)

    model.plot(forecast).savefig(f"{ticker}_plot.png")
    model.plot_components(forecast).savefig(f"{ticker}_plot_components.png")
    
    # tail return the last n rows
    #  and to_dict convert a df to a dictionnary with a 'record' orientation 
    return forecast.tail(days).to_dict("records")


def convert(prediction_list):
    output = {}
    for data in prediction_list:
        date = data["ds"].strftime("%m/%d/%Y")
        #create 2 columns in output with date and trend values
        output[date] = data["trend"]
    return output