# Deploying and Hosting a Machine Learning Model with FastAPI and Heroku

In this course I learnt what happens next after developing a model.
My goal was to understand what happens when a model gets deployed.
Now I can deploy a model myself and and make it available to end users.

You can have all the explanations related to this project and how to get the code on https://testdriven.io/blog/fastapi-machine-learning/

# Some troubles with the installation of fbprophet on Windows?

Follow the steps explained here : https://facebook.github.io/prophet/docs/installation.html

Don't forget to install pystan: https://pystan.readthedocs.io/en/latest/windows.html

if you may have somme trouble with matplotlib. 

In case my case I downgrate it from 3.3.1 to 3.0.3 to fix it.
 
More information here: https://github.com/matplotlib/matplotlib/issues/14558

Don't hesitate to contact me if needed. 